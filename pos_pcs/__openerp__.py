{
	'name': "Prepaid card system",
	'version': '1.0.0',
	'author': 'VSP',
	'category': 'Point Of Sale',
	'website': 'http://myodoo.ru',
	'depends': ['point_of_sale'],
	'license': 'Other proprietary',
	'data': [
		'views.xml',
	],

	'qweb': [

		'qweb.xml',
	],
}
