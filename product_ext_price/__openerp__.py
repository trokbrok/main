# -*- coding: utf-8 -*-
##############################################################################
#
#
##############################################################################


{
	'name': 'product_ext_price',
	'version': '0.1',
	'summary': 'Pricelist based on Avg Purchase price',
	'depends': ['product'],
	'license': 'Other proprietary',
	'description': """
		Pricelist based on Avg Purchase price
	""",

	'price': 1.00,
	'currency': 'EUR',

	'author': 'Vladimir Voronov',
	'installable': True,
	'data': [
		'product_view.xml',
		'user_data.xml',
	],
}
